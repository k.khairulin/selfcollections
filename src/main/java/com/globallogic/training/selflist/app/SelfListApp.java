package com.globallogic.training.selflist.app;

import com.globallogic.training.selflist.SelfArrayList;
import com.globallogic.training.selflist.SelfList;

public class SelfListApp {
	
	public static void main(String[] args) {
		SelfList<Integer> myList = new SelfArrayList<>();
		System.out.println("Initialized new SelfArrayList.");
		System.out.println("Is empty:");
		System.out.println(myList.isEmpty());
		myList.add(333);
		System.out.println("Is empty after adding 1 element");
		System.out.println(myList.isEmpty());
		myList.add(444);
		System.out.println("Is empty after adding another element:");
		System.out.println(myList.isEmpty());
		System.out.println("Current size:");
		System.out.println(myList.size());
		myList.add(888);
		System.out.println("Is empty after adding another element:");
		System.out.println(myList.isEmpty());
		System.out.println("Current size:");
		System.out.println(myList.size());
		Integer val = 999;
		myList.add(val);
		System.out.println("Check if list contains added value:");
		System.out.println(myList.contains(val));
		System.out.println("Check if list contains not added value");
		System.out.println(myList.contains(12345));
		Integer val2 = 19;
		myList.add(val2);
		System.out.println("Check if list contains added value:");
		System.out.println(myList.contains(val2));
		System.out.println("Check if list contains added value:");
		System.out.println(myList.contains(val));
		myList.remove(val);
		System.out.println("Check if list contains removed value:");
		System.out.println(myList.contains(val));
		myList.remove(val2);
		System.out.println("Check if list contains removed value:");
		System.out.println(myList.contains(val2));
		myList.add(17);
		myList.add(27);
		myList.add(37);
		myList.add(47);
		myList.add(57);
		myList.add(67);
		myList.add(77);
		System.out.println("Checking list values after filling initial capacity:");
		System.out.println(myList);
		myList.add(97);
		System.out.println("Checking list values after overfilling initial capacity:");
		System.out.println(myList);
		System.out.println("Checking index of element in list:");
		System.out.println(myList.indexOf(888));
		System.out.println("Checking index of element in list:");
		System.out.println(myList.indexOf(47));
		System.out.println("Checking element value before it was changed:");
		System.out.println(myList.set(6, 99));
		System.out.println("Checking if set method worked correctly:");
		System.out.println(myList);
		myList.set(1, 79);
		System.out.println("Checking if set method worked correctly:");
		System.out.println(myList);
		myList.clear();
		System.out.println("List after invoking clear once:");
		System.out.print("  size:");
		System.out.println(myList.size());
		System.out.print("  isEmpty:");
		System.out.println(myList.isEmpty());
		System.out.print("  list values:");
		System.out.println(myList);
		myList.clear();
		System.out.println("List after invoking clear twice:");
		System.out.print("  size:");
		System.out.println(myList.size());
		System.out.print("  isEmpty:");
		System.out.println(myList.isEmpty());
		System.out.print("  list values:");
		System.out.println(myList);
		
		
	}
}
