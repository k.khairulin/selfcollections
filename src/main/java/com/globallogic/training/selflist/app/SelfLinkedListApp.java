package com.globallogic.training.selflist.app;

import com.globallogic.training.selflist.SelfLinkedList;

public class SelfLinkedListApp {
	
	public static void main(String[] args) {
		SelfLinkedList<Integer> myList = new SelfLinkedList<>();
		myList.add(333);
		System.out.println(myList.isEmpty());
		myList.add(444);
		System.out.println(myList.isEmpty());
		System.out.println(myList.size());
		myList.add(888);
		System.out.println(myList.isEmpty());
		System.out.println(myList.size());
		Integer val = 999;
		myList.add(val);
		System.out.println(myList.contains(val));
		System.out.println(myList.contains(new Integer(2345)));
		Integer val2 = 19;
		myList.add(val2);
		System.out.println(myList.contains(val2));
		System.out.println(myList);
		System.out.println(myList.contains(val));
		myList.remove(val);
		System.out.println(myList.contains(val));
		System.out.println(myList);
		System.out.println(myList.remove(val2));	
		System.out.println(myList.contains(val2));
		System.out.println(myList);
		System.out.println(myList.indexOf(888));
		System.out.println(myList.set(1, 99));
		System.out.println(myList);
		System.out.println(myList.set(1, 79));
		System.out.println(myList);
		myList.clear();
		System.out.println(myList);
	}
}
