package com.globallogic.training.selflist;

import java.util.Iterator;

public class SelfLinkedList<E> implements SelfList<E>, Iterator<E>, Iterable<E> {
	
	private Node<E> first;
	private Node<E> last;
	private Node<E> position;
	
	private class Node<E> {
		E item;
		Node<E> next;
		Node<E> prev;
		
		Node(Node prev, E item, Node next) {
			this.prev = prev;
			this.item = item;
			this.next = next;
		}
	}

	@Override
	public int size() {
		int size = 0;
		Node<E> currentNode = first;
		if (currentNode == null) {
			return 0;
		}
		size++;
		while(currentNode.next != null) {
			size++;
			currentNode = currentNode.next;
		}
		return size;
	}

	@Override
	public boolean isEmpty() {
		return first == null;
	}

	@Override
	public boolean contains(Object o) {
		Node<E> currentNode = first;
		while(currentNode != null) {
			if (currentNode.item.equals(o)) {
				return true;
			}
			currentNode = currentNode.next;
		}
		return false;
	}

	@Override
	public boolean add(E e) {
		if (first == null) {
			first = new Node<E>(null, e, null);
			last = first;
			position = first;
		} else {
			last.next = new Node<E>(last, e, null);
			last = last.next;
		}
		return true;
	}

	@Override
	public boolean remove(Object o) {
		Node<E> currentNode = first;
		while(currentNode != null) {
			if (currentNode.item.equals(o)) {
				if (currentNode.equals(first)) {
					first = first.next;
					first.prev = null;
					currentNode = null;
				} else if (currentNode.equals(last)){
					last = last.prev;
					last.next = null;
					currentNode = null;
				} else {
					currentNode.prev.next = currentNode.next;
					currentNode.next.prev = currentNode.prev;
					currentNode = null;
				}
				return true;
			}
			currentNode = currentNode.next;
		}
		return false;
	}

	@Override
	public void clear() {
		first = null;
		last = null;
	}

	@Override
	public E get(int index) {
		Node<E> currentNode = first;
		for(int i = 0; i < index; i++) {
			currentNode = currentNode.next;
		}
		return currentNode.item;
	}

	@Override
	public E set(int index, E element) {
		Node<E> currentNode = first;
		for(int i = 0; i < index; i++) {
			currentNode = currentNode.next;
		}
		E item = currentNode.item;
		currentNode.item = element;
		return item;
	}

	@Override
	public E remove(int index) {
		Node<E> currentNode = first;
		for(int i = 0; i < index; i++) {
			currentNode = currentNode.next;
		}
		E item = currentNode.item;
		if (currentNode.equals(first)) {
			first = first.next;
			first.prev = null;
			currentNode = null;
		} else if (currentNode.equals(last)){
			last = last.prev;
			last.next = null;
			currentNode = null;
		} else {
			currentNode.prev.next = currentNode.next;
			currentNode.next.prev = currentNode.prev;
			currentNode = null;
		}
		return item;
	}

	@Override
	public int indexOf(Object o) {
		Node<E> currentNode = first;
		int index = 0;
		while(currentNode != null) {
			if (currentNode.item.equals(o)) {
				return index;
			}
			index++;
			currentNode = currentNode.next;
		}
		return -1;
	}

	public Node<E> getFirst() {
		return first;
	}

	public Node<E> getLast() {
		return last;
	}
	
	@Override
	public boolean hasNext() {
		return position.next != null;
	}

	@Override
	public E next() {
		position = position.next;
		return position.item;
	}

	@Override
	public Iterator<E> iterator() {
		return this;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		Node<E> currentNode = first;
		builder.append("SelfLinkedList: [ ");
		while(currentNode != null) {
			builder.append(String.format("{%s <- %s -> %s}, ",
					currentNode.prev != null? currentNode.prev.item : null,
					currentNode.item,
					currentNode.next != null? currentNode.next.item : null));
			currentNode = currentNode.next;
		}
		builder.append("]");
		return builder.toString();
	}
}
