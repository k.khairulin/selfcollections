package com.globallogic.training.selflist;

import java.util.Arrays;

public class SelfArrayList<E> implements SelfList<E> {
	private static final int DEFAULT_CAPACITY = 10;
	private Object[] elementData;
	private int currentCapacity;
	private int size;

	public SelfArrayList() {
		this.currentCapacity = DEFAULT_CAPACITY;
		this.elementData = new Object[DEFAULT_CAPACITY];
	}
	
	public SelfArrayList(int initialCapacity) {
		this();
		if(initialCapacity >= 0) {
			this.currentCapacity = initialCapacity;
			this.elementData = new Object[initialCapacity];
		}
	}

	public int size() {
		return this.size;
	}

	public boolean isEmpty() {
		return this.size == 0;
	}

	public boolean contains(Object o) {
		for(Object obj : this.elementData) {
			if (obj != null && obj.equals(o)) {
				return true;
			}
		}
		return false;
	}

	public boolean add(E e) {
		if (this.size == this.currentCapacity) {
			this.currentCapacity = (this.currentCapacity * 3/2) + 1;
			this.elementData = Arrays.copyOf(this.elementData, this.currentCapacity);
		}
		this.elementData[size] = e;
		this.size++;
		return true;
	}

	public boolean remove(Object o) {
		for(int i = 0; i < elementData.length; i++) {
			if (elementData[i] != null && elementData[i].equals(o)) {
				elementData[i] = null;
				this.size--;
				return true;
			}
		}
		return false;
	}

	public void clear() {
		if (size == 0) {
			this.currentCapacity = DEFAULT_CAPACITY;
			this.elementData = new Object[DEFAULT_CAPACITY];
		}
		for(int i = 0; i < this.elementData.length; i++) {
			this.elementData[i] = null;
		}
		this.size = 0;
	}

	public E get(int index) {
		return (E) this.elementData[index];
	}

	public E set(int index, E element) {
		E overridenElement = (E) this.elementData[index];
		this.elementData[index] = element;
		return overridenElement;
	}

	public E remove(int index) {
		E element = (E) this.elementData[index];
		this.elementData[index] = null;
		this.size--;
		return element;
	}

	public int indexOf(Object o) {
		for(int i = 0; i < this.elementData.length; i++) {
			if (this.elementData[i].equals(o)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public String toString() {
		return "SelfArrayList [elementData=" + Arrays.toString(elementData) + "]";
	}
	
	
	
}
