package com.globallogic.training.selfmap;

import java.util.ArrayList;
import java.util.List;

public class Main {
	
	public static void main(String[] args) {
		SelfHashMap<String, Integer> map = new SelfHashMap<>();
		map.put("One", 1);
		map.put("Two", 2);
		map.put("Three", 3);
		map.put("Four", 4);
		map.put("Five", 5);
		System.out.println(map.put("One", 10));
		System.out.println(map.put("Six", 6));
		System.out.println(map.get("One"));
		System.out.println(map.get("One"));
		System.out.println(map.get("Two"));
		System.out.println(map.get("Three"));
		System.out.println(map.get("Four"));
		System.out.println(map.get("Five"));
		System.out.println("Size " + map.size());
		map.remove("Two");
		System.out.println(map.get("Two"));
		System.out.println("Size " + map.size());
		System.out.println(map.get("Two"));
		map.put("Two", 2);
		System.out.println(map.get("Two"));
		System.out.println("Size " + map.size());
		map.put("Two", 2);
		System.out.println("Size " + map.size());
		map.put("Six", 6);
		map.put("Seven", 7);
		map.put("Eight", 8);
		map.put("Nine", 9);
		map.put("Ten", 10);
		map.put("Eleven", 11);
		map.put("Twelve", 12);
		map.put("Thirteen", 13);
		map.put("Fourteen", 14);
		map.put("Fifteen", 15);
		map.put("Sixteen", 16);
		map.put("Seventeen", 17);
		map.put("Eighteen", 18);
		map.put("Nineteen", 19);
		map.put("Twenty", 20);
		map.put("Yu", 21);
		map.put("Zi", 22);
		map.put("Xe", 23);
		map.put("Wi", 24);
		map.put("Qu", 25);
		map.put("Twenty-six", 26);
		map.put("Twenty-seven", 27);
		map.put("Twenty-eight", 28);
		map.put("Twenty-nine", 29);
		map.put("Thirty", 30);
		map.put("Thirty-one", 31);
		map.put("Thirty-two", 32);
		map.put("Thirty-three", 33);
		System.out.println("======================");
		System.out.println(map.get("asdasfasfasf"));
		System.out.println(map.remove("asdasd23gsg"));
		System.out.println(map.get("Thirty"));
		System.out.println(map.remove("Thirty"));
		System.out.println(map.get("Thirty"));
	}
}
