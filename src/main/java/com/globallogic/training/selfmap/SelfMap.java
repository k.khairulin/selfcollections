package com.globallogic.training.selfmap;

public interface SelfMap<K, V> {
	void clear();
	boolean containsKey(Object key);
	V get(Object key);
	boolean isEmpty();
	V put(K key, V value);
	V remove(Object key);
	int size();
}
