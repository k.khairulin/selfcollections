package com.globallogic.training.selfmap;

import java.util.LinkedList;
import java.util.Optional;

public class SelfHashMap<K, V> implements SelfMap<K, V> {
	
	private final int DEFAULT_INITIAL_CAPACITY = 16;
	private final double DEFAULT_LOAD_FACTOR = 0.75;
	private int currentCapacity;
	private LinkedList<Entry<K, V>> [] buckets;
	private int bucketsLoad;
	private int size;
	
	public SelfHashMap() {
		this.currentCapacity = DEFAULT_INITIAL_CAPACITY;
		this.buckets = (LinkedList<Entry<K, V>>[])new LinkedList [DEFAULT_INITIAL_CAPACITY];
	}
	
	private class Entry<K, V> {
		K key;
		V value;
		
		private Entry(K key, V value) {
			this.key = key;
			this.value = value;
		}
	}

	@Override
	public void clear() {
		for (LinkedList<Entry<K, V>> bucket : this.buckets) {
			bucket = null;
		}
		bucketsLoad = 0;
		size = 0;
	}

	@Override
	public boolean containsKey(Object key) {
		return findValueByKey(key).isPresent();
	}

	@Override
	public V get(Object key) {
		return findValueByKey(key).orElse(null);
	}
	
	@Override
	public boolean isEmpty() {
		return bucketsLoad == 0;
	}

	@Override
	public V put(K key, V value) {
		Optional<V> optionalValue = Optional.empty();
		int bucketIndex = getIndex(key);
		if (this.buckets[bucketIndex] == null) {
			if ((double)(bucketsLoad + 1)/currentCapacity >= DEFAULT_LOAD_FACTOR) {
				resize();
				bucketIndex = getIndex(key);
			}
			LinkedList<Entry<K, V>> bucketList = new LinkedList<>();
			this.buckets[bucketIndex] = bucketList;
			bucketList.add(new Entry<K,V>(key, value));
			bucketsLoad++;
			size++;
		} else {
			Optional<Entry<K, V>> optionalEntry = getEntryIfPresent(bucketIndex, key);
			if (!optionalEntry.isPresent()) {
				this.buckets[bucketIndex].add(new Entry<K, V>(key, value));
				size++;
			} else {
				Entry<K, V> entry = optionalEntry.orElse(null);
				optionalValue = Optional.of(entry.value);
				entry.value = value;
			}
		}
		return optionalValue.orElse(null);
	}

	@Override
	public V remove(Object key) {
		int index = getIndex((K)key);
		if (buckets[index] == null) {
			return null;
		}
		Optional<Entry<K, V>> optionalEntry = getEntryIfPresent(index, (K) key);
		optionalEntry.ifPresent(entry -> {
			buckets[index].remove(entry);
			if (buckets[index].isEmpty()) {
				buckets[index] = null;
				bucketsLoad--;
			}
			size--;
		});
		return optionalEntry.map(entry -> entry.value).orElse(null);
	}

	@Override
	public int size() {
		return size;
	}
	
	private int getIndex(K key) {
		return key.hashCode() & (currentCapacity - 1);
	}
	
	private Optional<V> findValueByKey(Object key) {
		Optional<V> optionalValue = Optional.empty();
		int index = getIndex((K)key);
		if (buckets[index] != null) {
			optionalValue = getEntryIfPresent(index, (K)key).map(entry -> entry.value);
		}
		return optionalValue;
	}
	
	private Optional<Entry<K, V>> getEntryIfPresent(int index, K key) {
		Optional<Entry<K, V>> optionalEntry = Optional.empty();
		for (Entry<K, V> entry : buckets[index]) {
			if (key.hashCode() == entry.key.hashCode() && key.equals(entry.key)) {
				optionalEntry = Optional.of(entry);
			}
		}
		return optionalEntry;
	}
	
	private void resize() {
		bucketsLoad = 0;
		size = 0;
		LinkedList<Entry<K, V>> [] bucketsCopy = buckets;
		currentCapacity = currentCapacity * 2;
		buckets = (LinkedList<Entry<K, V>>[])new LinkedList [currentCapacity];
		for (LinkedList<Entry<K, V>> bucket : bucketsCopy) {
			if (bucket != null) {
				for(Entry<K, V> entry : bucket) {
					put(entry.key, entry.value);
				}
			}
		}
	}
}
