package com.globallogic.training.selfset;

public class Main {
	
	public static void main(String[] args) {
		
		SelfHashSet<String> set = new SelfHashSet<>();
		System.out.println(set.isEmpty());
		System.out.println(set.add("One"));
		System.out.println(set.add("Two"));
		System.out.println(set.add("Three"));
		System.out.println(set.contains("One"));
		System.out.println(set.size());
		System.out.println(set.add("Two"));
		System.out.println(set.size());
		System.out.println(set.remove("Two"));
		System.out.println(set.size());
		System.out.println(set.contains("Two"));
	}
}
