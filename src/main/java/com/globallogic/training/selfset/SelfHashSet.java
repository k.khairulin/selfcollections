package com.globallogic.training.selfset;

import com.globallogic.training.selfmap.SelfHashMap;

public class SelfHashSet<E> implements SelfSet<E>{
	
	private SelfHashMap<E, Object> map = new SelfHashMap<>();
	private final Object DUMMY = new Object();
	
	@Override
	public boolean add(E e) {
		return map.put(e, DUMMY) == null;
	}
	
	@Override
	public void clear() {
		map.clear();
	}
	
	@Override
	public boolean contains(Object o) {
		return map.containsKey(o);
	}
	
	@Override
	public boolean remove(Object o) {
		return map.remove(o) == DUMMY;
	}
	
	@Override
	public boolean isEmpty() {
		return map.isEmpty();
	}
	
	@Override
	public int size() {
		return map.size();
	}
	
	
}
