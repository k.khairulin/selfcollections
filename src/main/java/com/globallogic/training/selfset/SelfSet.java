package com.globallogic.training.selfset;

public interface SelfSet<E> {
	
	boolean add(E e);
	void clear();
	boolean contains(Object o);
	boolean remove(Object o);
	boolean isEmpty();
	int size();
}
